# helm-charts

```bash
helm repo add segfault-labs https://segfault-labs.gitlab.io/helm-charts/
```

## postgresql-clients

**values.yaml** (choose postgresql client version)

```yaml
image:
  tag: 11
```

```bash
helm upgrade --install --wait --atomic --timeout 10m \
  --create-namespace \
  --set image.tag=11 \
  --namespace=psql \
  psql-11 segfault-labs/postgresql-client
```

## oracle-client

```bash
helm upgrade --install --wait --atomic --timeout 10m \
  --create-namespace \
  --namespace=oracle \
  oracle-18-5 segfault-labs/oracle-client
```
