# dcgm-exporter

![Version: 3.1.9](https://img.shields.io/badge/Version-3.1.9-informational?style=flat-square) ![AppVersion: 3.1.7](https://img.shields.io/badge/AppVersion-3.1.7-informational?style=flat-square)

A Helm chart for DCGM exporter

**Homepage:** <https://github.com/nvidia/dcgm-exporter/>

## Source Code

* <https://github.com/nvidia/dcgm-exporter>

## Requirements

Kubernetes: `>= 1.19.0-0`

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| arguments[0] | string | `"-f"` |  |
| arguments[1] | string | `"/etc/dcgm-exporter/dcp-metrics-included.csv"` |  |
| extraConfigMapVolumes | list | `[]` |  |
| extraEnv | list | `[]` |  |
| extraHostVolumes | list | `[]` |  |
| extraVolumeMounts | list | `[]` |  |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"nvcr.io/nvidia/k8s/dcgm-exporter"` |  |
| image.tag | string | `"3.2.5-3.1.7-ubuntu20.04"` |  |
| imagePullSecrets | list | `[]` |  |
| kubeletPath | string | `"/var/lib/kubelet/pod-resources"` |  |
| nameOverride | string | `""` |  |
| namespaceOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| resources | object | `{}` |  |
| rollingUpdate.maxSurge | int | `0` |  |
| rollingUpdate.maxUnavailable | int | `1` |  |
| runtimeClassName | string | `""` |  |
| securityContext.capabilities.add[0] | string | `"SYS_ADMIN"` |  |
| securityContext.runAsNonRoot | bool | `false` |  |
| securityContext.runAsUser | int | `0` |  |
| service.address | string | `":9400"` |  |
| service.annotations | object | `{}` |  |
| service.enable | bool | `true` |  |
| service.port | int | `9400` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `true` |  |
| serviceAccount.name | string | `nil` |  |
| serviceMonitor.additionalLabels | object | `{}` |  |
| serviceMonitor.enabled | bool | `true` |  |
| serviceMonitor.honorLabels | bool | `false` |  |
| serviceMonitor.interval | string | `"15s"` |  |
| serviceMonitor.relabelings | list | `[]` |  |
| tolerations | list | `[]` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.0](https://github.com/norwoodj/helm-docs/releases/v1.11.0)
